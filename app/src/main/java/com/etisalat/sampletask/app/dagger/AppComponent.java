package com.etisalat.sampletask.app.dagger;

import android.content.Context;

import com.etisalat.sampletask.app.dagger.modules.AppModule;
import com.etisalat.sampletask.app.dagger.modules.NetworkModule;
import com.etisalat.sampletask.app.network.FoodsNetwork;
import com.google.gson.Gson;

import dagger.Component;
import okhttp3.OkHttpClient;

@AppScope
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {

    Context context();
    OkHttpClient okhttpClient();
    Gson gson();
    FoodsNetwork foodsNetwork();

}
