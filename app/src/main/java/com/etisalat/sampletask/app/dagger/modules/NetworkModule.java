package com.etisalat.sampletask.app.dagger.modules;

import android.content.Context;
import java.io.File;
import com.etisalat.sampletask.app.dagger.AppScope;
import com.etisalat.sampletask.app.network.FoodsNetwork;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import timber.log.Timber;

@Module
public class NetworkModule {

    @AppScope
    @Provides
    public Cache cache(Context context) {
        return new Cache(new File(context.getCacheDir(), "okhttp_cache"),
                10 * 1024 * 1024);
    }

    @AppScope
    @Provides
    public HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
                message -> Timber.d(message));
        return logging.setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @AppScope
    @Provides
    public OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor, Cache cache) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .cache(cache)
                .build();
    }

    @AppScope
    @Provides
    public Gson gson(){
        return new Gson();
    }

    @AppScope
    @Provides
    public Retrofit retrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://api.androidhive.info/pizza/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @AppScope
    @Provides
    public FoodsNetwork foodsNetwork(Retrofit retrofit) {
        return retrofit.create(FoodsNetwork.class);
    }

}
