package com.etisalat.sampletask.app.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface FoodsNetwork {

    //    ?format=xml

    @GET(".")
    Call<ResponseBody> getFoods(@Query("format")String format);

    @GET(".")
    Observable<Response<ResponseBody>> getFoodsRx(@Query("format")String format);

}
