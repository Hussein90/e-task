package com.etisalat.sampletask.app;

import android.app.Activity;
import android.app.Application;

import com.etisalat.sampletask.BuildConfig;
import com.etisalat.sampletask.app.dagger.AppComponent;
import com.etisalat.sampletask.app.dagger.DaggerAppComponent;
import com.etisalat.sampletask.app.dagger.modules.AppModule;

import timber.log.Timber;

public class ApplicationTask extends Application {

    public static ApplicationTask get(Activity activity) {
        return (ApplicationTask) activity.getApplication();
    }

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected void log(int priority, String tag, String message, Throwable t) {
                    super.log(priority, " SAMPLE_TASK : ", message, t);
                }
            });
        }

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent component() {
        return appComponent;
    }
}
