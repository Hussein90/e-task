package com.etisalat.sampletask.activities.foods.dagger;

import com.etisalat.sampletask.activities.foods.FoodsActivity;
import com.etisalat.sampletask.app.dagger.AppComponent;

import dagger.Component;

@FoodScope
@Component(modules = {FoodModule.class}, dependencies = AppComponent.class)
public interface FoodComponent {

    void inject(FoodsActivity foodsActivity);

}
