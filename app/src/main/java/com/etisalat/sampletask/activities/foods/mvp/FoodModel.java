package com.etisalat.sampletask.activities.foods.mvp;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import com.etisalat.sampletask.activities.foods.FoodsActivity;
import com.etisalat.sampletask.app.network.FoodsNetwork;
import com.etisalat.sampletask.app.network.model.Food;
import com.etisalat.sampletask.data.FoodsDBHelper;
import com.twistedequations.rxstate.RxSaveState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import timber.log.Timber;

public class FoodModel {

    public static final String FOODS_STATE_KEY = "saved_foods_key";
    private final FoodsActivity activity;

    private FoodsNetwork foodsNetwork;

    public FoodModel(FoodsActivity activity, FoodsNetwork foodsNetwork) {
        this.activity = activity;
        this.foodsNetwork = foodsNetwork;
    }

    public Observable<Response<ResponseBody>> getFoodsResponse() {
        return foodsNetwork.getFoodsRx("xml");
    }


    public void updateDB(ArrayList<Food> data) {
        Uri uri;
        ContentValues cv;
        uri = FoodsDBHelper.CONTENT_URI;
        activity.getContentResolver().delete(uri, null, null);
        for (Food f : data) {
            cv = new ContentValues();
            cv.put(FoodsDBHelper.COL_ID, f.getId());
            cv.put(FoodsDBHelper.COL_NAME, f.getName());
            cv.put(FoodsDBHelper.COL_DESCRIPTION, f.getDescription());
            cv.put(FoodsDBHelper.COL_COST, f.getCost());
            activity.getContentResolver().insert(FoodsDBHelper.CONTENT_URI, cv);
        }
    }

    public ArrayList<Food> getCachedFoodsList() {
        ArrayList<Food> foods = new ArrayList<>(0);
        Cursor c = activity.getContentResolver().query(FoodsDBHelper.CONTENT_URI,
                FoodsDBHelper.COLUMNS,
                null,
                null,
                null);
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                Food f = new Food();
                f.setId(c.getInt(0));
                f.setName(c.getString(1));
                f.setDescription(c.getString(2));
                f.setCost(c.getString(3));
                foods.add(f);
                c.moveToNext();
            }
        }
        c.close();
        return foods;
    }


    public void saveFoodsState(ArrayList<Food> list) {
        RxSaveState.updateSaveState(activity, bundle -> {
            bundle.putParcelableArrayList(FOODS_STATE_KEY, list);
        });
    }

    public Observable<List<Food>> getFoodsFromSaveState() {
        return RxSaveState.getSavedState(activity)
                .map(bundle -> bundle.getParcelableArrayList(FOODS_STATE_KEY));
    }

    public void getFoods(FoodPresenter presenter) {
        foodsNetwork.getFoods("xml").enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                actionsOnResponse(presenter, response);
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.e(t);
                presenter.showSnackbar("Network Error!");
            }
        });
    }
    public void getFoodsRx(FoodPresenter presenter, Response<ResponseBody> response){
        actionsOnResponse(presenter, response);
    }

    private ArrayList<Food> actionsOnResponse(FoodPresenter presenter, Response<ResponseBody> response){
        ArrayList<Food> foods = new ArrayList<>(0);
        try {
            String xmlString = response.body().string();
            JSONObject jsonObj = XML.toJSONObject(xmlString);
            JSONArray jsonArray = jsonObj.getJSONObject("menu").getJSONArray("item");

            for(int i=0 ; i < jsonArray.length() ; i++){
                JSONObject o = jsonArray.getJSONObject(i);
                Food f = new Food();
                f.setId(o.getInt("id"));
                f.setName(o.getString("name"));
                f.setCost(o.getString("cost")+"\nL.E");
                f.setDescription(o.getString("description"));
                foods.add(f);
            }
            // [1] Sorting the List.
            Collections.sort(foods, (f1, f2) -> f1.getName().compareTo(f2.getName()));
            // [2] Cache the List.
            updateDB(foods);
            // [3] Refresh the RecyclerView with the new List.
            presenter.refreshRecyclerView(foods);
            Timber.i(jsonObj.toString());
            return foods;
        } catch (IOException e) {
            e.printStackTrace();
            return foods;
        } catch (JSONException e) {
            Timber.e(e.getMessage());
            return foods;
        }
    }
}

