package com.etisalat.sampletask.activities.foods.dagger;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.etisalat.sampletask.activities.foods.FoodsActivity;
import com.etisalat.sampletask.activities.foods.mvp.FoodModel;
import com.etisalat.sampletask.activities.foods.mvp.FoodPresenter;
import com.etisalat.sampletask.activities.foods.mvp.FoodView;
import com.etisalat.sampletask.app.network.FoodsNetwork;
import com.etisalat.sampletask.bases.BasePresenterListener;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module
public class FoodModule {

    private final FoodsActivity activity;

    public FoodModule(FoodsActivity activity) {
        this.activity = activity;
    }

    @Provides
    @FoodScope
    public FoodView view() {
        return new FoodView(activity);
    }

    @Provides
    @FoodScope
    public FoodModel model(FoodsNetwork foodsNetwork) {
        return new FoodModel(activity,foodsNetwork);
    }

    @Provides
    @FoodScope
    public FoodPresenter presenter(FoodView view, FoodModel model) {
        return new FoodPresenter(activity, view, model);
    }

}
