package com.etisalat.sampletask.activities.foods.mvp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.view.View;

import com.etisalat.sampletask.activities.foods.FoodsActivity;
import com.etisalat.sampletask.activities.picture.PictureActivity;
import com.etisalat.sampletask.app.network.model.Food;
import com.etisalat.sampletask.bases.BasePresenter;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class FoodPresenter extends BasePresenter {

    private final FoodsActivity activity;
    private final FoodView view;
    private final FoodModel model;
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();

    public FoodPresenter(FoodsActivity foodsActivity, FoodView view, FoodModel model) {
        super(foodsActivity);
        this.activity = foodsActivity;
        this.view = view;
        this.model = model;
    }

    public void onCreate() {
        compositeSubscription.add(observeRefresh());
        if (!isOnline())
            showSnackbar("No internet connection!");
        ArrayList<Food> list = model.getCachedFoodsList();
        if (list.isEmpty())
            view.activity.showSnackbar("There's no data to show, CLICK Refresh!", view.getRootView());
        else {
            view.adapter.setFoodsList(list);
            view.rvFoods.setAdapter(view.adapter);
        }
    }

    // Button click handling.
    public void refreshBtnClicked(View v) {
        if (!isOnline())
            showSnackbar("No internet connection!");
        activity.showProgress();
        model.getFoods(this);
    }

    public void pictureBtnClicked(View v) {
        Intent intent = new Intent(activity, PictureActivity.class);
        activity.startActivity(intent);
    }

    public void onDestroy() {
        compositeSubscription.clear();
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    private Subscription loadSavedState() {
        return model.getFoodsFromSaveState()
                .subscribe(gitHubRepoList -> view.setMessage("Data Loaded!"));
    }

    // Rx Button click handling.
    private Subscription observeRefresh() {
        return view.observeBtnRefresh()
                .doOnNext(__ -> view.showLoading())
                .observeOn(Schedulers.io())
                .switchMap(__ -> model.getFoodsResponse())
                .doOnNext(response -> model.getFoodsRx(this, response))
                .observeOn(AndroidSchedulers.mainThread())
//                .doOnNext(model::saveFoodsState)
                .doOnEach(__ -> view.hideLoading())
                .retry()
                .subscribe(/*response -> model.getFoodsRx(this, response)*/);
    }

    public void refreshRecyclerView(ArrayList<Food> list) {
        view.setFoodsToRecyclerView(list);
        activity.hideProgress();
    }

    public void showSnackbar(String message) {
        activity.showSnackbar(message, view.getRootView());
    }
}
