package com.etisalat.sampletask.activities.foods.mvp;

import android.app.Activity;
import android.database.Cursor;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;
import com.etisalat.sampletask.R;
import com.etisalat.sampletask.activities.foods.FoodsActivity;
import com.etisalat.sampletask.activities.foods.adapter.FoodsAdapter;
import com.etisalat.sampletask.app.network.model.Food;
import com.etisalat.sampletask.data.FoodsDBHelper;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;

public class FoodView extends FrameLayout {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btnRefresh)
    ImageButton btnRefresh;

    @BindView(R.id.btnPicture)
    ImageButton btnPicture;

    @BindView(R.id.rvFoods)
    RecyclerView rvFoods;
    FoodsAdapter adapter;

    FoodsActivity activity;

    private FoodPresenter presenter;
    public void setPresenter(FoodPresenter presenter) {
        this.presenter = presenter;
    }

    public FoodView(FoodsActivity activity) {
        super(activity);
        this.activity = activity;
        inflate(getContext(), R.layout.activity_foods, this);
        ButterKnife.bind(this);
        rvFoods.setHasFixedSize(true);
        rvFoods.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new FoodsAdapter(activity);
        rvFoods.setAdapter(adapter);
        // Handled by Rx, to active this please hash this line of code
        // number[38] in FoodPresenter.java
//        btnRefresh.setOnClickListener(v ->{
//            presenter.refreshBtnClicked(v);
//        });
        btnPicture.setOnClickListener(v ->{
            presenter.pictureBtnClicked(v);
        });
    }

    public void setFoodsToRecyclerView(ArrayList<Food> list){
        adapter.setFoodsList(list);
        rvFoods.setAdapter(adapter);
    }

    public Observable<Void> observeBtnRefresh() {
        return RxView.clicks(btnRefresh);
    }


    public void setMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void showLoading(){
        activity.showProgress();
    }
    public void hideLoading(){
        activity.hideProgress();
    }

}
