package com.etisalat.sampletask.activities.foods;

import android.os.Bundle;import com.etisalat.sampletask.activities.foods.dagger.DaggerFoodComponent;
import com.etisalat.sampletask.activities.foods.dagger.FoodModule;
import com.etisalat.sampletask.activities.foods.mvp.FoodPresenter;
import com.etisalat.sampletask.activities.foods.mvp.FoodView;
import com.etisalat.sampletask.app.ApplicationTask;
import com.etisalat.sampletask.bases.BaseActivity;
import com.etisalat.sampletask.bases.BasePresenter;

import javax.inject.Inject;

public class FoodsActivity extends BaseActivity{

    @Inject
    FoodView view;
    @Inject
    FoodPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerFoodComponent.builder()
                .appComponent(ApplicationTask.get(this).component())
                .foodModule(new FoodModule(this))
                .build().inject(this);
        view.setPresenter(presenter);
        setContentView(view);
        presenter.onCreate();

    }

    @Override
    protected BasePresenter setupPresenter() {
        return presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
