package com.etisalat.sampletask.activities.foods.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.etisalat.sampletask.R;
import com.etisalat.sampletask.activities.foods.FoodsActivity;
import com.etisalat.sampletask.app.network.model.Food;

import java.util.ArrayList;
import java.util.List;

public class FoodsAdapter extends RecyclerView.Adapter<FoodsAdapter.FoodViewHolder> {

    private FoodsActivity activity;
    private List<Food> foodsList = new ArrayList<>(0);

    public FoodsAdapter(FoodsActivity activity) {
        this.activity = activity;
    }

    public void setFoodsList(List<Food> foodsList) {
        this.foodsList = foodsList;
    }

    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_food, parent, false);
        return new FoodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder holder, int position) {
        holder.tvName.setText(foodsList.get(position).getName());
        holder.tvDescription.setText(foodsList.get(position).getDescription());
        holder.tvCost.setText(foodsList.get(position).getCost());
    }

    @Override
    public int getItemCount() {
        return foodsList.size();
    }


    class FoodViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvDescription, tvCost;

        public FoodViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvCost = (TextView) itemView.findViewById(R.id.tvCost);
        }
    }
}


