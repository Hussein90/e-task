package com.etisalat.sampletask.activities.foods.dagger;

import javax.inject.Scope;

@Scope
public @interface FoodScope {
}
