package com.etisalat.sampletask.bases;

import com.etisalat.sampletask.activities.foods.FoodsActivity;

public abstract class BasePresenter<T extends BaseController,
        E extends BasePresenterListener>
        implements BaseControllerListener {

    protected E listener;
    protected T controller;

    public BasePresenter(E listener) {
        this.listener = listener;
    }
}