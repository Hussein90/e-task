package com.etisalat.sampletask.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class FoodsDBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "foods.db";
    private static final int DB_VERSION = 3;
    public static final String TABLE_NAME = "favorites";
    public static final String COL_ID = "food_id";
    public static final String COL_NAME = "name";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_COST = "cost";
    public static final String COUNT = "COUNT(*)";

    public static final String[] COLUMNS = new String[]{
            COL_ID, COL_NAME, COL_DESCRIPTION, COL_COST};

    public static final String AUTHORITY = "com.etisalat.sampletask";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    public FoodsDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_WAITLIST_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID+ " INTEGER ," +
                COL_NAME + " TEXT ," +
                COL_DESCRIPTION + " TEXT ," +
                COL_COST + " TEXT " +
                "); ";
        db.execSQL(SQL_CREATE_WAITLIST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
