package com.etisalat.sampletask;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static junit.framework.Assert.assertEquals;

public class SortingUnitTest {

    @Test
    public void testSoting() {
        ArrayList<String> array = new ArrayList<String>();
        array.add("Samy");
        array.add("Hany");
        array.add("Fady");
        ArrayList<String> expectedArray = new ArrayList<String>();
        expectedArray .add("Fady");
        expectedArray .add("Hany");
        expectedArray .add("Samy");
        Collections.sort(array);
        assertEquals(expectedArray,array);
    }
}
